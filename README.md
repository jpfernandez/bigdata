# BigData

This repository stores the source code of the Kaggle's competitions in which I have participated; some models from the repository haven't been submited for the competition.

The source code is written in Python using the libraries: Numpy, Pandas, Scikit.

## Competitions
### Bike Sharing Demand
http://www.kaggle.com/c/bike-sharing-demand

The model consists of:

* Feature engineering/selection
* Imputation of missing data
* Prediction for test data using ExtraTreesClassifier.

The score for this model is 0.86118 on public leaderboard evaluated on RMSLE.

[folder code](https://bitbucket.org/jpfernandez/bigdata/src/801d7379594c3895e571671d9ac10f27a5cb3a4a/BigData/src/BikeRental)

### Forest Cover Type Prediction
http://www.kaggle.com/c/forest-cover-type-prediction

The model consists of:

* Feature engineering/selection.
* Prediction for test data using ExtraTreesClassifier.

The model scores 0.80817 on competition's public leaderboard.

[folder code](https://bitbucket.org/jpfernandez/bigdata/src/71726c519bc916809d1fcfb9fdcc05790d9535e6/BigData/src/ForestCover/)

### Allstate Purchase Prediction Challenge
https://www.kaggle.com/c/allstate-purchase-prediction-challenge

The model consists of:

* Data imputation of missing values using regression.
* Calculation of the probability of the purchased quote of each customer.
* Selection of the purchased quote of each customer based on the previous calculation.

[folder code](https://bitbucket.org/jpfernandez/bigdata/src/71726c519bc916809d1fcfb9fdcc05790d9535e6/BigData/src/Allstate/)

### PAKDD 2014 - ASUS Malfunctional Components Prediction
https://www.kaggle.com/c/pakdd-cup-2014

The model consists of:

* Map/Reduce summarization data of the same type from dataset, using mincemeat.py implementation of Map/Reduce.
* Identification and imputation of missing data.
* Construction of train and test data.
* Prediction for test data using RandomForest.

The model scores 19.91494 on public leaderboard evaluated on the mean absolute error (MAE) 

[folder code](https://bitbucket.org/jpfernandez/bigdata/src/71726c519bc916809d1fcfb9fdcc05790d9535e6/BigData/src/Asus/)

### Digit Recognizer
https://www.kaggle.com/c/digit-recognizer

The model consists of:

* Loading train and test data, getting the histogram of each image and appending it to the data.
* Dimensionality Reduction (PCA)
* Making prediction of test data using ExtraTressClassifier.

The model scores 0.96514 on competition's public leaderboard.

[folder code](https://bitbucket.org/jpfernandez/bigdata/src/71726c519bc916809d1fcfb9fdcc05790d9535e6/BigData/src/Digit/)


### Titanic: Machine Learning from Disaster
https://www.kaggle.com/c/titanic-gettingStarted

The model consists of:

* Loading train and test data imputing missing values for the label age with GaussianNB (Naive Bayes)
* Making prediction for test data using RandomForest.

[folder code](https://bitbucket.org/jpfernandez/bigdata/src/71726c519bc916809d1fcfb9fdcc05790d9535e6/BigData/src/Titanic/)