import csv as csv 
import numpy as np
#import matplotlib.pyplot as pl
import pylab as pl
from sklearn import linear_model
import re

def findAge():
    csv_file= csv.reader(open('Data/train.csv', 'rb')) 
    header = csv_file.next()  #The next() command just skips the first line which is a header
    data=[]                          #Create a variable called 'data'
    for row in csv_file:      #Run through each row in the csv file
        data.append(row)             #adding each row to the data variable
    data = np.array(data)              #Then convert from a list to an array be aware that each item is currently a string in this format
    
    # copy of data
    copy = data.copy()
    
    column0 = data[ : ,0].astype(np.float) #survived
    column1 = data[ : ,1].astype(np.float) #pclass  
    column2 = []                           #psex  
    index_column2 = 0
    for row in copy[ : ,3]:
        if row == "female":
            row = np.float(0)
        if row == "male":
            row = np.float(1)    
        column2.insert(index_column2, row)
        index_column2 += 1

    column3 = data[ : ,4]                  #age
    column4 = data[ : ,5].astype(np.float) #sibsp
    column5 = data[ : ,6].astype(np.float) #parch
    column6 = []                            #ticket
    index_column6 = 0
    prog = re.compile("\s\d+")
    prog2 = re.compile("\d+")
    for row in copy[ : ,7]:
        if not prog2.match(row):
            exp1 = prog.findall(row)
            exp2 = prog2.findall(''.join(exp1))
            exp2 = ''.join(exp2)
        if prog2.match(row):
            exp2 = row
        else:
            exp2 = '0'
        
        column6.insert(index_column6,np.float(exp2))
        index_column6 += 1
    
    column7 = data[ : ,8].astype(np.float) #fare
    column8 = []                        #embarqued: Cherbourg, Southamption and Queenstown
    index_column8 = 0
    for row in copy[ : ,9]:
        if row == "C":
            row = np.float(0)
        if row == "S":
            row = np.float(1)    
        if row == "Q":
            row = np.float(2)
        else:
            row = np.float(1)    
        column8.insert(index_column8, row)
        index_column8 += 1
    
    train_data = np.array([column3,column0,column1,column2,column4,column5,column6,column7,column8])
    
    auxiliar = []
    rows = train_data.shape[1]
    ind = 0
    while ind < rows:
        #print train_data[ 1:: ,ind]
        #if train_data[ 1:: ,ind][6] != float(-1) and train_data[ 1:: ,ind][2] != float(-1):
        auxiliar.insert(ind, train_data[  : ,ind])
        ind +=1
        
    con_edad = []
    sin_edad = []
    
    for row in auxiliar[ : ]:
        if row[0] == "":
            row[0] = -1
            sin_edad.append(row)
        else:
            con_edad.append(row)
            
 
    target = np.array(sin_edad)
    train = np.array(con_edad)
    
    train = train.astype(np.float)
    target = target.astype(np.float)
    target_sin_edad = target[ : ,1:]
    
    clf = linear_model.LinearRegression()
    clf.fit(train[ : ,1:], train[ : ,0])
    edades = clf.predict(target_sin_edad)
        
    target[ : ,0] = edades
    
    c = np.concatenate((train,target))

    new_train = np.array([c[ : ,1],c[ : ,2],c[ : ,3],c[ : ,0],c[ : ,4],c[ : ,5],c[ : ,6],c[ : ,7],c[ : ,8]])
    return new_train

        
def main():    
    findAge()

if __name__=="__main__":
    main()        