import csv as csv 
import numpy as np
import re
#Open up the csv file in to a Python object
def loadTestData():
    csv_file_object = csv.reader(open('Data/test.csv', 'rb')) 
    header = csv_file_object.next()  #The next() command just skips the first line which is a header
    data=[]                          #Create a variable called 'data'
    for row in csv_file_object:      #Run through each row in the csv file
        data.append(row)             #adding each row to the data variable
    data = np.array(data)              #Then convert from a list to an array be aware that each item is currently a string in this format
    
    # copy of data
    copy = data.copy()
    
    age_males_3=[]
    age_males_2=[]
    age_males_1=[]
    age_females_3=[]
    age_females_2=[]
    age_females_1=[]
    
    for row in copy[ : ,1:5]:
        if row[2] == "male" and row[0]=="3" and row[3] != "":
            age_males_3.append(np.float(row[3]))
        if row[2] == "male" and row[0]=="2" and row[3] != "":
            age_males_2.append(np.float(row[3]))
        if row[2] == "male" and row[0]=="1" and row[3] != "":
            age_males_1.append(np.float(row[3]))
        if row[2] == "female" and row[0]=="3" and row[3] != "": 
            age_females_3.append(np.float(row[3]))
        if row[2] == "female" and row[0]=="2" and row[3] != "": 
            age_females_2.append(np.float(row[3]))
        if row[2] == "female" and row[0]=="1" and row[3] != "": 
            age_females_1.append(np.float(row[3]))
    
    med_age_male_1 = sum(age_males_1)/age_males_1.__len__()
    med_age_male_2 = sum(age_males_2)/age_males_2.__len__()
    med_age_male_3 = sum(age_males_3)/age_males_3.__len__()
    med_age_female_1 = sum(age_females_1)/age_females_1.__len__()   
    med_age_female_2 = sum(age_females_2)/age_females_2.__len__()
    med_age_female_3 = sum(age_females_3)/age_females_3.__len__()

    column0 = data[ : ,1].astype(np.float) #pclass  
    column1 = []                        #psex  
    index_column1 = 0
    for row in copy[ : ,3]:
        if row == "female":
            row = np.float(0)
        if row == "male":
            row = np.float(1)    
        column1.insert(index_column1, row)
        index_column1 += 1
    
    column2 = []                        #age
    index_column2 = 0
    for row in copy[ : ,1:5]:
        if row[2] == "male" and row[0]=="3" and row[3] == "":
            row[3] = med_age_male_3
        if row[2] == "male" and row[0]=="2" and row[3] == "":
            row[3] = med_age_male_2
        if row[2] == "male" and row[0]=="1" and row[3] == "":
            row[3] = med_age_male_1
        if row[2] == "female" and row[0]=="3" and row[3] == "": 
            row[3] = med_age_female_3
        if row[2] == "female" and row[0]=="2" and row[3] == "": 
            row[3] = med_age_female_2
        if row[2] == "female" and row[0]=="1" and row[3] == "": 
            row[3] = med_age_female_1
        column2.insert(index_column2, np.float(row[3]))
        index_column2 += 1
        
    column3 = data[ : ,5].astype(np.float) #sibsp
    column4 = data[ : ,6].astype(np.float) #parch
    column5 = []                            #ticket
    index_column5 = 0
    prog = re.compile("\s\d+")
    prog2 = re.compile("\d+")
    for row in copy[ : ,7]:
        if not prog2.match(row):
            exp1 = prog.findall(row)
            exp2 = prog2.findall(''.join(exp1))
            exp2 = ''.join(exp2)
        if prog2.match(row):
            exp2 = row
        else:
            exp2 = '0'
        column5.insert(index_column5, np.float(exp2))
        index_column5 += 1
    
    column6 = [] #data[ : ,7]           #fare
    index_column6 = 0
    for row in copy[ : ,8]:
        if row == "":
            row = np.float(15)  
        else:
            row = np.float(row)
        column6.insert(index_column6,row)
        index_column6 += 1
        
    column7 = []                        #embarqued: Cherbourg, Southamption and Queenstown
    index_column7 = 0
    for row in copy[ : ,10]:
        if row == "C":
            row = np.float(0)
        if row == "S":
            row = np.float(1)    
        if row == "Q":
            row = np.float(2)    
        column7.insert(index_column7, row)
        index_column7 += 1
        
    column8 = []
    index_column8 = 0
    for row in copy[ : ,2]:
        tipo = row.split(',')[1].strip() 
        if tipo.startswith('Mr') or tipo.startswith('Mrs') or tipo.startswith('Dona') or tipo.startswith('Rev') or tipo.startswith('Don') or tipo.startswith('Major') or tipo.startswith('Capt') or tipo.startswith('Jonkheer') or tipo.startswith('Col') or tipo.startswith('Countess') or tipo.startswith('Mme') or tipo.startswith('Dr'):
            adulto = np.float(1)
        else:
            adulto = np.float(0)
        column8.insert(index_column8, adulto)
        index_column8 += 1
               
    target = np.array([column0,column1,column2,column3,column4,column5,column6,column7,column8])
    print target
    return target.astype(np.float)