#import FindAgeTrain, FindAgeTest
import loadTest, loadTrain
# Import the random forest package
import FindAgeTrain, FindAgeTest
#import loadTest, loadTrain
# Import the random forest package
from sklearn.cross_validation import cross_val_score
from sklearn.ensemble.forest import RandomForestClassifier
import csv as csv
import numpy as np 

aux = []
aux2 = []
#train_data = loadTrain.loadTrainData()
#test_data = loadTest.loadTestData()
train_data = FindAgeTrain.loadAndFindAge()
test_data = FindAgeTest.loadAndFindAge()



def data():
    #print sys.float_info
    #print train_data[0].shape
    rows = train_data.shape[1]
    ind = 0
    while ind < rows:
        #print train_data[ 1:: ,ind]
        aux.insert(ind, train_data[ 1:: ,ind])
        ind +=1

    rows = test_data.shape[1]
    ind = 0
    while ind < rows:
        #print test_data[ : ,ind]
        aux2.insert(ind, test_data[ : ,ind])
        ind +=1
        
    nptrain = np.array(aux)
    nptarget = np.array(aux2)
    #print nptarget[ : ,3]

def predict():        
    #print aux2        
    #print aux2[0][5]    
    # Create the random forest object which will include all the parameters for the fit
    Forest = RandomForestClassifier(n_estimators = 100,n_jobs=2,random_state=1512)
    
    scores = cross_val_score(Forest, train_data, train_data[ : ,0])
    print scores.mean()

    # Fit the training data to the training output and create the decision trees
    #Forest = Forest.fit(train_data[0::,1::],train_data[0::,0])

    Forest = Forest.fit(aux,train_data[0])
    # Take the same decision trees and run on the test data
    Output = Forest.predict(aux2)
    print Output
    return Output
    
    #savetxt('Data/submission.csv', Output, delimiter=',', fmt='%f')
def save_predict(Output):    
    open_file_object = csv.writer(open("Data/RandomForestAgeBayesAdultIdSoloId.csv", "wb"))
    j=0
    index = 892
    open_file_object.writerow(["PassengerId","Survived"])
    for row in Output:
        open_file_object.writerow([str(index),str(int(Output[j]))])
        j += 1
        index += 1
    
def main():    
    data()
    datos = predict()
    save_predict(datos)

if __name__=="__main__":
    main()