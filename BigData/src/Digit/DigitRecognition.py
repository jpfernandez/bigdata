import LoadData
from sklearn.cross_validation import cross_val_score
from sklearn.ensemble.forest import RandomForestClassifier
from sklearn.ensemble.forest import ExtraTreesClassifier
from sklearn.linear_model import SGDClassifier
from sklearn.naive_bayes import BernoulliNB
from sklearn.decomposition import RandomizedPCA
import csv as csv

def recognition():
    rdo = []
    rdo = LoadData.loadData()
    
    X = rdo.__getitem__(0)
    y = X[ : ,0]
    Z = rdo.__getitem__(1)
    #print Z
    
    rForest = RandomForestClassifier(n_estimators=10, max_depth=None,random_state=0, n_jobs=2)
    sgd = SGDClassifier(loss="hinge", penalty="l2",n_jobs=2)
    naiveBayes = BernoulliNB()
    extraTrees = ExtraTreesClassifier(n_estimators=100, max_depth=None, random_state=1512)
  

    pca = RandomizedPCA(n_components=10)
    #pca = decomposition.PCA()
    X = pca.fit_transform(X[ : ,1:])
    Z = pca.transform(Z)
    
    '''
    for clf in [rForest,extraTrees]:
        scores = cross_val_score(clf, X, y)
        print scores.mean()
    '''
    
    extraTrees = extraTrees.fit(X,y)
    output = extraTrees.predict(Z)
    
    return output
    
def save_predict(Output):    
    open_file_object = csv.writer(open("Data/extraTreesHistoPCA.csv", "wb"))
    j=0
    index = 1
    open_file_object.writerow(["ImageId","Label"])
    for row in Output:
        open_file_object.writerow([str(index),str(int(Output[j]))])
        j += 1
        index += 1
    

def main():
    out = recognition()
    save_predict(out)
    
if __name__=="__main__":
    main()