import csv as csv
import numpy as np


def loadData():
    csv_file_object = csv.reader(open('Data/train.csv', 'rb')) 
    header = csv_file_object.next()  #The next() command just skips the first line which is a header
    data=[]                          #Create a variable called 'data'
    for row in csv_file_object:      #Run through each row in the csv file
        #data.append(row)             #adding each row to the data variable
        histo = np.histogram(np.array(row).astype(np.int0), bins = range(256))
        #print histo
        map(np.int0, row) + histo[0].tolist()
        data.append(row+histo[0].tolist())
    data = np.array(data).astype(np.int0)              #Then convert from a list to an array be aware that each item is currently a string in this format
    train = data.copy()

    csv_file_object = csv.reader(open('Data/test.csv', 'rb')) 
    header = csv_file_object.next()  #The next() command just skips the first line which is a header   data=[]                          #Create a variable called 'data'
    data=[]
    for row in csv_file_object:      #Run through each row in the csv file
        #data.append(row)             #adding each row to the data variable
        histo = np.histogram(np.array(row).astype(np.int0), bins = range(256))
        #print histo
        map(np.int0, row) + histo[0].tolist()
        data.append(row+histo[0].tolist())
    data = np.array(data).astype(np.int0)              #Then convert from a list to an array be aware that each item is currently a string in this format
    test = data.copy()
    '''
    i=0
    tam = test.shape[1]
    while i < tam:
        print i
        print test[ : ,i] - test[ : ,i+1]
        i+=1
    '''
    return [train,test]

def main():
    loadData()

if __name__=="__main__":
    main()

