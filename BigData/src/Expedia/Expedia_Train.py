import csv as csv 
import numpy as np

csv_file_object = csv.reader(open('train_0.csv', 'rb')) 
header = csv_file_object.next()  #The next() command just skips the first line which is a header
data=[]                          #Create a variable called 'data'
for row in csv_file_object:      #Run through each row in the csv file
    data.append(row)             #adding each row to the data variable
data = np.array(data)              #Then convert from a list to an array be aware that each item is currently a string in this format

# copy of data
copy = data.copy()
preferences = []
index_column = 0

search_ids = data[ : ,0].astype(np.float) #search_id
prop_ids = data[ : ,7].astype(np.float) #prop_id
                             
index_column = 0
for row in copy[ : ,8]:                 #prop_starring
    preferences.insert(index_column, np.float(row))
    index_column += 1
    
index_column = 0
for row in copy[ : ,9]:                 #prop_review_score
    if(row == 'NULL'):
        row = '0'
    preferences.insert(index_column, np.float(row) + preferences[index_column])
    index_column += 1

index_column = 0
for row in copy[ : ,10]:                 #prop_band_bool
    if(row == 'NULL'):
        row = '0'
    preferences.insert(index_column, np.float(row) + preferences[index_column])
    index_column += 1
        
index_column = 0
for row in copy[ : ,11]:                 #prop_location_scrore1
    if(row == 'NULL'):
        row = '0'
    preferences.insert(index_column, np.float(row) + preferences[index_column])
    index_column += 1

index_column = 0
for row in copy[ : ,12]:                 #prop_location_scrore2
    if(row == 'NULL'):
        row = '0'
    if(row.find('.') == -1):
        row = np.float(row) / 10000
    else:
        row = np.float(row)
        
    preferences.insert(index_column, row + preferences[index_column])
    index_column += 1
    
index_column = 0
for row in copy[ : ,13]:                 #prop_log_historical_price
    if(row == 'NULL'):
        row = '0'
    preferences.insert(index_column, np.float(row) + preferences[index_column])
    index_column += 1
    
index_column = 0
for row in copy[ : ,14]:                 #prop_log_historical_price
    if(row == 'NULL'):
        row = '0'
    preferences.insert(index_column, np.float(row) + preferences[index_column])
    index_column += 1          

index_column = 0
for row in copy[ : ,17]:                 #promotion_flag
    if(row == 'NULL'):
        row = '0'
    preferences.insert(index_column, np.float(row) + preferences[index_column])
    index_column += 1          

index_column = 0
for row in copy[ : ,19]:                 #search_length_stay
    if(row == 'NULL'):
        row = '0'
    preferences.insert(index_column, np.float(row) + preferences[index_column])
    index_column += 1      

index_column = 0
for row in copy[ : ,21]:                 #srch_length_of_stay
    if(row == 'NULL'):
        row = '0'
    preferences.insert(index_column, np.float(row) + preferences[index_column])
    index_column += 1 
    
index_column = 0
for row in copy[ : ,22]:                 #srch_booking_window
    if(row == 'NULL'):
        row = '0'
    preferences.insert(index_column, np.float(row) + preferences[index_column])
    index_column += 1

index_column = 0
for row in copy[ : ,23]:                 #srch_adults_count
    if(row == 'NULL'):
        row = '0'
    preferences.insert(index_column, np.float(row) + preferences[index_column])
    index_column += 1 
    
index_column = 0
for row in copy[ : ,24]:                 #srch_children_count
    if(row == 'NULL'):
        row = '0'
    preferences.insert(index_column, np.float(row) + preferences[index_column])
    index_column += 1    

index_column = 0
for row in copy[ : ,25]:                 #srch_room_count
    if(row == 'NULL'):
        row = '0'
    preferences.insert(index_column, np.float(row) + preferences[index_column])
    index_column += 1 
    
index_column = 0
for row in copy[ : ,26]:                 #srch_saturday_night_bool
    if(row == 'NULL'):
        row = '0'
    preferences.insert(index_column, np.float(row) + preferences[index_column])
    index_column += 1

index_column = 0
for row in copy[ : ,30]:                 #comp1_rate
    if(row == 'NULL'):
        row = '0'
    preferences.insert(index_column, np.float(row) + preferences[index_column])
    index_column += 1

index_column = 0
for row in copy[ : ,31]:                 #comp1_inv
    if(row == 'NULL'):
        row = '0'
    preferences.insert(index_column, np.float(row) + preferences[index_column])
    index_column += 1
    
index_column = 0
for row in copy[ : ,32]:                 #comp1_rate_percent_diff
    if(row == 'NULL'):
        row = '0'
    preferences.insert(index_column, np.float(row) + preferences[index_column])
    index_column += 1    

index_column = 0
for row in copy[ : ,33]:                 #comp2_rate
    if(row == 'NULL'):
        row = '0'
    preferences.insert(index_column, np.float(row) + preferences[index_column])
    index_column += 1
    
index_column = 0
for row in copy[ : ,34]:                 #comp2_inv
    if(row == 'NULL'):
        row = '0'
    preferences.insert(index_column, np.float(row) + preferences[index_column])
    index_column += 1    
    
index_column = 0
for row in copy[ : ,35]:                 #comp2_rate_percent_diff
    if(row == 'NULL'):
        row = '0'
    preferences.insert(index_column, np.float(row) + preferences[index_column])
    index_column += 1

index_column = 0
for row in copy[ : ,36]:                 #comp3_rate
    if(row == 'NULL'):
        row = '0'
    preferences.insert(index_column, np.float(row) + preferences[index_column])
    index_column += 1    

index_column = 0
for row in copy[ : ,37]:                 #comp3_inv
    if(row == 'NULL'):
        row = '0'
    preferences.insert(index_column, np.float(row) + preferences[index_column])
    index_column += 1
    
index_column = 0
for row in copy[ : ,38]:                 #comp3_rate_percent_diff
    if(row == 'NULL'):
        row = '0'
    preferences.insert(index_column, np.float(row) + preferences[index_column])
    index_column += 1

index_column = 0
for row in copy[ : ,39]:                 #comp4_rate
    if(row == 'NULL'):
        row = '0'
    preferences.insert(index_column, np.float(row) + preferences[index_column])
    index_column += 1
    
index_column = 0
for row in copy[ : ,40]:                 #comp4_inv
    if(row == 'NULL'):
        row = '0'
    preferences.insert(index_column, np.float(row) + preferences[index_column])
    index_column += 1
    
index_column = 0
for row in copy[ : ,41]:                 #comp4_rate_percent_diff
    if(row == 'NULL'):
        row = '0'
    preferences.insert(index_column, np.float(row) + preferences[index_column])
    index_column += 1    

index_column = 0
for row in copy[ : ,42]:                 #comp5_rate
    if(row == 'NULL'):
        row = '0'
    preferences.insert(index_column, np.float(row) + preferences[index_column])
    index_column += 1     
    
index_column = 0
for row in copy[ : ,43]:                 #comp5_inv
    if(row == 'NULL'):
        row = '0'
    preferences.insert(index_column, np.float(row) + preferences[index_column])
    index_column += 1
    
index_column = 0
for row in copy[ : ,44]:                 #comp5_rate_percent_diff
    if(row == 'NULL'):
        row = '0'
    preferences.insert(index_column, np.float(row) + preferences[index_column])
    index_column += 1

index_column = 0
for row in copy[ : ,45]:                 #comp6_rate
    if(row == 'NULL'):
        row = '0'
    preferences.insert(index_column, np.float(row) + preferences[index_column])
    index_column += 1

index_column = 0
for row in copy[ : ,46]:                 #comp6_inv
    if(row == 'NULL'):
        row = '0'
    preferences.insert(index_column, np.float(row) + preferences[index_column])
    index_column += 1

index_column = 0
for row in copy[ : ,47]:                 #comp6_rate_percent_diff
    if(row == 'NULL'):
        row = '0'
    preferences.insert(index_column, np.float(row) + preferences[index_column])
    index_column += 1

index_column = 0
for row in copy[ : ,48]:                 #comp7_rate
    if(row == 'NULL'):
        row = '0'
    preferences.insert(index_column, np.float(row) + preferences[index_column])
    index_column += 1

index_column = 0
for row in copy[ : ,49]:                 #comp7_inv
    if(row == 'NULL'):
        row = '0'
    preferences.insert(index_column, np.float(row) + preferences[index_column])
    index_column += 1

index_column = 0
for row in copy[ : ,50]:                 #comp7_rate_percent_diff
    if(row == 'NULL'):
        row = '0'
    preferences.insert(index_column, np.float(row) + preferences[index_column])
    index_column += 1

index_column = 0
for row in copy[ : ,51]:                 #comp8_rate
    if(row == 'NULL'):
        row = '0'
    preferences.insert(index_column, np.float(row) + preferences[index_column])
    index_column += 1

index_column = 0
for row in copy[ : ,52]:                 #comp8_inv
    if(row == 'NULL'):
        row = '0'
    preferences.insert(index_column, np.float(row) + preferences[index_column])
    index_column += 1

index_column = 0
for row in copy[ : ,53]:                 #comp8_rate_percent_diff
    if(row == 'NULL'):
        row = '0'
    preferences.insert(index_column, np.float(row) + preferences[index_column])
    index_column += 1

# cada valor de preference lo divido entre el numero de features utilizados: 40; para restar peso a estos features
index_column = 0
for row in preferences:                 
    if(row == 'NULL'):
        row = '0'
    preferences.insert(index_column, preferences[index_column] / 40) 
    index_column += 1

# los siguientes features tienen mayor peso    
index_column = 0
for row in copy[ : ,27]:                 #srch_query_affinity_score
    if(row == 'NULL'):
        row = '0'
    preferences.insert(index_column, np.float(row) + preferences[index_column])
    index_column += 1

index_column = 0
for row in copy[ : ,54]:                 #click_bool
    if(row == 'NULL'):
        row = '0'
    preferences.insert(index_column, np.float(row) + preferences[index_column])
    index_column += 1        

index_column = 0
for row in copy[ : ,56]:                 #booking_bool
    if(row == 'NULL'):
        row = '0'
    preferences.insert(index_column, np.float(row) + preferences[index_column])
    index_column += 1 

# 29 random_bool

new_train = np.array([search_ids],[prop_ids],[preferences])
print new_train

open_file_object = csv.writer(open('preferences_0.csv', 'wb'))
open_file_object.writerow(['srch_id','prop_id','preferences'])
for row in new_train:
    open_file_object.writerow(row)

