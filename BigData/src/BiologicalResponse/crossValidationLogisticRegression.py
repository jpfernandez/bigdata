from sklearn import linear_model
from numpy import genfromtxt, savetxt
    
def main():    
    logistic = linear_model.LogisticRegression()
    
    dataset = genfromtxt(open('Data/train.csv','r'), delimiter=',', dtype='f8')[1:]    
    target = [x[0] for x in dataset]
    train = [x[1:] for x in dataset]
    test = genfromtxt(open('Data/test.csv','r'), delimiter=',', dtype='f8')[1:]
    
    X = train
    y = target
    
    # Prediction
    predicted_probs = [x[1] for x in logistic.fit(X, y).predict_proba(test)]
    savetxt('Data/submission.csv', predicted_probs, delimiter=',', fmt='%f')
    
if __name__=="__main__":
    main()    