import pandas as pd
import numpy as np
from numpy import fft
from matplotlib import pyplot as plt
from sklearn.ensemble.forest import RandomForestClassifier
from sklearn.linear_model import SGDClassifier
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.naive_bayes import BernoulliNB
from sklearn import neighbors
from sklearn.cross_validation import cross_val_score
import sklearn.feature_selection as fs
from sklearn.decomposition import RandomizedPCA

# filter type
HIGH_FILTER_TYPE = 'high'
LOW_FILTER_TYPE = 'low'
# numeric type of soil
N_TOPSOIL = float(1)
N_SUBSOIL = float(9)

def pass_filter(x,type):
    samples = x.__len__()
    """ fft based brute force low pass filter """
    a = fft.rfft(x)
    tot = len(a)
    for x in xrange(tot-samples):
        a[samples + x] = 0.0
    if type == HIGH_FILTER_TYPE:
        '''Compute the one-dimensional discrete Fourier Transform.'''
        return fft.rfft(a) 
    else:
        if type == LOW_FILTER_TYPE:
            '''Compute the one-dimensional inverse discrete Fourier Transform.'''
            return fft.irfft(a)


def transform_data(z,num):
    '''
    Sustituir Topsoil por 1, y Subsoil por 0,
    Pasar filtro paso alto/bajo a todos los espectros
    '''
    aux_df_train = pd.DataFrame()
    column_names = list(z.columns.values)
    num_features_not_spectum = num
    i=1 # excluir id (primera columna)
    while i < column_names.__len__() - num_features_not_spectum :
        a = z.ix[:,i:i+1].apply(lambda x : N_TOPSOIL if str(x) == 'Topsoil' else x)
        b = a.apply(lambda x : N_SUBSOIL if str(x) == 'Subsoil' else x)
        x = b.values.flatten()
        rdo = pass_filter(x,LOW_FILTER_TYPE)
        aux_df_train[column_names[i]] = pd.Series(rdo)
        i+=1
    return aux_df_train
    
def load_data():
    dfTrain = pd.read_csv('data/training.csv', sep=',', header=0)#[0:10000]
    dfTest = pd.read_csv('data/sorted_test.csv', sep=',', header=0)#[0:10000]
    
    excluded_cols_train = ['BSAN','BSAS','BSAV','CTI','ELEV','EVI','LSTD','LSTN','REF1','REF2','REF3','REF7','RELI','TMAP','TMFI','Depth','Ca','P','pH','SOC','Sand']
    excluded_cols_test = ['BSAN','BSAS','BSAV','CTI','ELEV','EVI','LSTD','LSTN','REF1','REF2','REF3','REF7','RELI','TMAP','TMFI','Depth','PIDN']
       
    trans_train = transform_data(dfTrain,excluded_cols_train.__len__())
    trans_test = transform_data(dfTest,excluded_cols_test.__len__())
    '''
       add excluded columns for pass filter computation
       BSAN,BSAS,BSAV,CTI,ELEV,EVI,LSTD,LSTN,REF1,REF2,REF3,REF7,RELI,TMAP,TMFI,Depth,Ca,P,pH,SOC,Sand
    '''
    #example(trans_test.ix[:,4:5].values.flatten(),LOW_FILTER_TYPE)
    
    trans_train[excluded_cols_train] = dfTrain[excluded_cols_train]
    trans_train['Depth'] = trans_train['Depth'].apply(lambda x : N_TOPSOIL if str(x) == 'Topsoil' else N_SUBSOIL)
    
    trans_test[excluded_cols_test] = dfTest[excluded_cols_test]
    trans_test['Depth'] = trans_test['Depth'].apply(lambda x : N_TOPSOIL if str(x) == 'Topsoil' else N_SUBSOIL)
    
    predict(trans_test,trans_train)
    
    

def predict(trans_test,trans_train):
    n = len(trans_test.columns)
    print n
    cols = trans_test.ix[:,0:n-1]
    labels = trans_train[['Ca','P','pH','SOC','Sand']].values    # y
    features = trans_train[list(cols)].values     # X
    features_y = trans_test[list(cols)].values    # y   
    et = ExtraTreesClassifier(n_estimators=20,random_state=0)
    knn = neighbors.KNeighborsClassifier(15, weights="distance") #uniform
    sgd = SGDClassifier(loss="hinge", penalty="l2")
    '''
    pca = RandomizedPCA(n_components=10)
    #pca = decomposition.PCA()
    y = labels
    X = pca.fit_transform(features)
    #Z = pca.transform()
    '''
    #['accuracy', 'adjusted_rand_score', 'average_precision', 'f1', 'log_loss', 'mean_squared_error', 'precision', 'r2', 'recall', 'roc_auc']
    ''' MSE = 0: perfect prediction; MSE < 0: sub-prediction; MSE > 0: over-prediction '''
    print cross_val_score(knn, features, labels,scoring='mean_squared_error').mean()
    knn = knn.fit(features,labels)
    output = knn.predict(features_y)
    
    resultado = pd.DataFrame(data=np.array(output),columns=['Ca','P','pH','SOC','Sand'])
    #resultado = pd.DataFrame(data=trans_test['PIDN'])
    #resultado['PIDN'] = trans_test['PIDN']
    resultado.insert(0, 'PIDN', trans_test['PIDN'])
    #resultado[['Ca','P','pH','SOC','Sand']] = np.array(output)
    print resultado
    resultado.to_csv('data/Resultado.csv',index=False,sep=',')
    
    
def example(x,type):
    rdo = pass_filter(x,type)
    plt.plot(x,label='original')
    plt.plot(rdo,label='calculado')
    plt.hlines(0,0,2)
    plt.legend(loc=0)
    plt.show()

def main():
    #example()
    load_data()
   
if __name__=="__main__":
    main()  