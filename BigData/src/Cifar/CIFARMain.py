import glob
import os
import csv
from PIL import Image
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
import CIFARRandomForest

# variables globales
labels = {'airplane':1,'automobile':2,'bird':3,'cat':4,'deer':5,'dog':6,'frog':7,'horse':8,'ship':9,'truck':10}
imgLabels = {}

# for testing libs
def test():
    # matplot
    frog1=mpimg.imread('Data/1.png')
    truck=mpimg.imread('Data/2.png')
    frog20=mpimg.imread('Data/20.png')

    #plt.hist(frog1.flatten(), 256, range=(0.0,1.0), fc='k', ec='k')
    #a = plt.hist(frog1.flatten(), 256, range=(0.0,1.0), fc='k', ec='k')
    b = plt.hist(truck.flatten(), 256, range=(0.0,1.0), fc='k', ec='k')
    print b[0][0]
    print b[1][0]
    print b[2][0]
    plt.show()

def loadLabels():
    csv_file_object = csv.reader(open('Data/trainLabels.csv', 'rb'))    # id, label
    header = csv_file_object.next()  
    for row in csv_file_object:  
        idLabel = labels.get(row[1])
        imgLabels[row[0]] = idLabel            
    #print imgLabels

def loadImages(tipo):
    
    open_file_object = open("Data/" + tipo +".csv", "w")
    
    img_files = glob.glob('/Kaggle/CIFAR/Data/'+ tipo +'/1*.png')
    for file in img_files:
        # se obtiene el nombre de la imagen para su identificacion
        filename = os.path.basename(file)
        name = filename.split('.')[0]
        
        # se convierte la imagen a escala de grises y se obtienen los valore de sus pixeles e histograma
        img = Image.open(file).convert('LA')
        dataimg = list(img.getdata())
        histoimg = img.histogram()
        rowImg = ''
        for pixel in dataimg:
            #print pixel.__getitem__(0)
            rowImg = rowImg + str(pixel.__getitem__(0)) + ','
            
        rowHisto = ''
        for scale in histoimg:
            #print scale
            rowHisto = rowHisto + str(scale) + ','
        # segun el nombre de la imagen se obtiene a que clase pertenece la imagen
        rowForCSV = rowImg + rowHisto
        # quitar la ultima coma
        rowForCSV = rowForCSV[:-1]
        clase = imgLabels.get(name)
        #print str(clase) + ',' + rowForCSV
        if tipo.__eq__('train'): 
            open_file_object.write(str(clase) + ',' + rowForCSV + '\n')
        else:
            open_file_object.write(rowForCSV + '\n')
    print 'Fichero ' + tipo + ' creado.'   

def createTrainFile():
    loadLabels()
    loadImages('train')
    
def createTestFile():
    loadImages('test')

def main():
    createTrainFile()
    createTestFile()
    CIFARRandomForest.recognition()

if __name__ == '__main__':
    main()