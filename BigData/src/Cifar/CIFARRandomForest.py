'''
Created on 23/01/2014

@author: adm
'''
import numpy as np
import csv
from sklearn.cross_validation import cross_val_score
from sklearn.ensemble.forest import RandomForestClassifier
from sklearn.ensemble.forest import ExtraTreesClassifier
from sklearn import svm

def recognition():
    # load train data
    csv_file_object = csv.reader(open('Data/train.csv', 'rb')) 
    data=[]                          #Create a variable called 'data'
    for row in csv_file_object:      #Run through each row in the csv file
        data.append(row)             #adding each row to the data variable
    train = np.array(data).astype(np.float)           #Then convert from a list to an array be aware that each item is currently a string in this format    
    X = train
    y = X[ : ,0]
    
    # load test data
    csv_file_object = csv.reader(open('Data/test.csv', 'rb')) 
    data=[]                          #Create a variable called 'data'
    for row in csv_file_object:      #Run through each row in the csv file
        data.append(row)             #adding each row to the data variable
    test = np.array(data).astype(np.float)           #Then convert from a list to an array be aware that each item is currently a string in this format    
    Z = test
        
    #Z = test

    
    rForest = RandomForestClassifier(n_estimators=10, max_depth=20,min_samples_split=1, random_state=0, n_jobs=2)
    extraTrees = ExtraTreesClassifier(n_estimators=10, max_depth=20,min_samples_split=1, random_state=1512)
    a = svm.SVC()
    
    for clf in [rForest]:
        scores = cross_val_score(clf, X, y)
        print scores.mean()
    
if __name__ == '__main__':
    recognition()    
    