import numpy as np
import csv as csv
import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.ensemble.forest import RandomForestClassifier
from sklearn.linear_model import SGDClassifier
from sklearn.decomposition import TruncatedSVD
'''
csv_file= csv.reader(open('data/train.csv', 'rb'))
reader = csv_file.next()  #The next() command just skips the first line which is a header
data=[]                          #Create a variable called 'data'
for row in csv_file:      #Run through each row in the csv file
    data.append(row)             #adding each row to the data variable
t = np.array(data)              #Then convert from a list to an array be aware that each item is currently a string in this format
'''

'''
csv_file= csv.reader(open('data/test.csv', 'rb'))
reader = csv_file.next()  #The next() command just skips the first line which is a header
data=[]                          #Create a variable called 'data'
for row in csv_file:      #Run through each row in the csv file
    data.append(row)             #adding each row to the data variable
t2 = np.array(data)              #Then convert from a list to an array be aware that each item is currently a string in this format
'''
t = pd.read_csv('data/train.csv', sep=',', header=0)[0:1000]
t2 = pd.read_csv('data/test.csv', sep=',', header=0)[0:1000]

categories = [
    "I can't tell"
    ,"Negative"
    ,"Neutral / author is just sharing information"
    ,"Positive"
    ,"Tweet not related to weather condition"  
    ,"current (same day) weather"
    ,"future (forecast)"
    ,"I can't tell"
    ,"past weather"
    ,"clouds"
    ,"cold"
    ,"dry"
    ,"hot"
    ,"humid"
    ,"hurricane"
    ,"I can't tell"
    ,"ice"
    ,"other"
    ,"rain"
    ,"snow"
    ,"storms"
    ,"sun"
    ,"tornado"
    ,"wind"
]


tfidf = TfidfVectorizer(strip_accents='unicode', analyzer='word',stop_words="english")
tfidf.fit(t["tweet"])
X = tfidf.transform(t["tweet"])
test = tfidf.transform(t2["tweet"])
print tfidf.get_feature_names()
y = np.array(t.ix[:,4:])

lsa = TruncatedSVD(n_components=400, algorithm='randomized', n_iterations=5, random_state=None, tol=0.0)
#pca = decomposition.PCA()
X = lsa.fit_transform(X)
test = lsa.transform(test)

clf = RandomForestClassifier()
print X

clf.fit(X,y)
test_prediction = clf.predict(test)
print test_prediction

#RMSE:
print 'Train error: {0}'.format(np.sqrt(np.sum(np.array(np.array(clf.predict(X))-y)**2)/ (X.shape[0]*24.0)))


'''
count_vect = CountVectorizer(min_df=2,strip_accents='unicode', analyzer='word',stop_words="english")
X_train_counts = count_vect.fit_transform(t["tweet"])

tf_transformer = TfidfTransformer(use_idf=False).fit(X_train_counts)
X_train_tf = tf_transformer.transform(X_train_counts)

tfidf_transformer = TfidfTransformer()
X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)
print X_train_tfidf

#clf = MultinomialNB().fit(X_train_tfidf, t["s3"].astype(np.int))
clf = SGDClassifier().fit(X_train_tfidf, t["s3"])

X_new_counts = count_vect.transform(t2["tweet"])
X_new_tfidf = tfidf_transformer.transform(X_new_counts)

predicted = clf.predict(X_new_tfidf)

for s in predicted:
    if s != 0:
        print s

#print predicted
'''
