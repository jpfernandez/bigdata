import LoadData
#import Orange
import numpy as np
import csv as csv
from sklearn.cross_validation import cross_val_score
from sklearn.ensemble.forest import RandomForestClassifier
from sklearn.ensemble.forest import ExtraTreesClassifier
from sklearn import svm
'''
    Prediction of missing train sales
'''
    
def predictMissingSales():
    rdo = []
    rdo = LoadData.loadDataForMissingSales()
    X = rdo.__getitem__(0)
    y = X[ : ,0]
    Z = rdo.__getitem__(1)
    
    #clf = RandomForestClassifier(n_estimators=500,random_state=1512)
    clf = svm.SVC() 
    '''
    scores = cross_val_score(clf, X, y)
    print scores.mean()
    '''
    clf.fit(X[ : ,1:],y)
    out = clf.predict(Z)
    return out
    
'''
    Prediction of components to repair
'''
def addPredictions(ary):
    
    results = open('data/Results.csv','wb')
    cont=0
    with open('data/CustomTest.csv','rb') as csvCus:
        cus = csv.reader(csvCus, delimiter=',', quotechar=' ')    
        for row in cus:
            results.write(str(int(ary[cont]))+','+row[0]+','+row[1]+','+row[2]+','+row[3]+','+row[4]+','+row[5]+','+row[6]+'\n')
            cont+=1
       
def predict():
    #results = open('data/Prediction.csv','wb')
    print 'loading data...'
    rdo = []
    rdo = LoadData.loadData()
    print 'predicting...'
    X = rdo.__getitem__(0)
    y = X[ : ,0]
    Z = rdo.__getitem__(1)

    rForest = RandomForestClassifier(n_estimators=100,random_state=1512)
    rForest.fit(X[ : ,1:],y)
    
    out1 = rForest.predict(Z[0:20000])
    out2 = rForest.predict(Z[20000:40000])
    out3 = rForest.predict(Z[40000:60000])
    out4 = rForest.predict(Z[60000:80000])
    out5 = rForest.predict(Z[80000:100000])
    out6 = rForest.predict(Z[100000:120000])
    out7 = rForest.predict(Z[120000:140000])
    out8 = rForest.predict(Z[140000:160000])
    out9 = rForest.predict(Z[160000:180000])
    out10 = rForest.predict(Z[180000:200000])
    out11 = rForest.predict(Z[200000:210000])
    out12 = rForest.predict(Z[210000:220000])
    out13 = rForest.predict(Z[220000: ])
    
    #ary = np.memmap('data/ary', dtype='int', mode='r+', shape=(236968,1))
    ary = []
    ary = np.append(ary,np.array(out1).astype(int))
    ary = np.append(ary,np.array(out2).astype(int))
    ary = np.append(ary,np.array(out3).astype(int))
    ary = np.append(ary,np.array(out4).astype(int))
    ary = np.append(ary,np.array(out5).astype(int))
    ary = np.append(ary,np.array(out6).astype(int))
    ary = np.append(ary,np.array(out7).astype(int))
    ary = np.append(ary,np.array(out8).astype(int))
    ary = np.append(ary,np.array(out9).astype(int))
    ary = np.append(ary,np.array(out10).astype(int))
    ary = np.append(ary,np.array(out11).astype(int))
    ary = np.append(ary,np.array(out12).astype(int))
    ary = np.append(ary,np.array(out13).astype(int))
        
    addPredictions(ary)
    
    
    '''
    for clf in [rForest]:
        scores = cross_val_score(clf, X,y)
        print scores.mean()

    train = Orange.data.Table('data/CustomTrain.tab')
    test = Orange.data.Table('data/CustomTest.tab')
    #learner = Orange.classification.tree.TreeLearner()    #0.25
    #learner = Orange.ensemble.forest.RandomForestLearner(trees=20, name="forest")    #0.29
    #learner = Orange.classification.bayes.NaiveLearner(name="Naive Bayes")    #0.29
    #learner = Orange.classification.svm.SVMLearner()    
    classifier = learner(train)
    
    #res = Orange.evaluation.testing.cross_validation([learner], train, folds=5)
    #print "Accuracy: %.2f" % Orange.evaluation.scoring.CA(res)[0]
    #print "AUC:      %.2f" % Orange.evaluation.scoring.AUC(res)[0]
  
    
    i = 0
    for d in test[0:]:
        c = classifier(d)
        print "%10s; originally %s" % (classifier(d), d.getclass())
        results.write(str(classifier(d))+','+str(test[i][0])+','+str(test[i][1])+','+str(test[i][2])+','+str(test[i][3])+','+str(test[i][4])+','+str(test[i][5])+','+str(test[i][6])+'\n')
        i+=1
    '''

def main():
    predict()         

if __name__=="__main__":
    main()
           
