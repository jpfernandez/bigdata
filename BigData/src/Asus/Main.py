import Prediction
import MissingSales
import DB
import mincemeat
import csv as csv
import sqlite3
import numpy as np

#variables globales
medias = {}

def mapfnSale(k, v):
    #module    component    fec_sale    -->    n_sales
    yield v[0]+'#'+v[1]+'#'+v[2],int(v[3])
    print v[0]+'#'+v[1]+'#'+v[2]+'#'+str(int(v[3]))
    #for w in v.split():
    #    yield w, 1
def reducefnSale(k, vs):
    result = sum(vs)
    return result

def mapfnRepair(k, v):
    #module    component    fec_sale    fec_repair    -->    n_repair
    yield v[0]+'#'+v[1]+'#'+v[2]+'#'+v[3],int(v[4])
    print v[0]+'#'+v[1]+'#'+v[2]+'#'+v[3]+'#'+str(int(v[4]))
    #for w in v.split():
    #    yield w, 1
def reducefnRepair(k, vs):
    result = sum(vs)
    return result

def createCustomSale():
    conn = sqlite3.connect("data/asus.db")
    fileTrainSale = open("data/CustomTrainSale.csv", "wb")
    data=[]
    with open('data/SaleTrain.csv', 'rb') as csvfileS:
        sale = csv.reader(csvfileS, delimiter=',', quotechar=' ')
        sale.next()
        for row in sale:
            data.append(row)
    print "Data loaded."    
    datasource = dict(enumerate(data))
        
    s = mincemeat.Server()
    s.datasource = datasource
    s.mapfn = mapfnSale
    s.reducefn = reducefnSale
    results = s.run_server(password="changeme")
    for k,v in results.items():
        tokens = k.split('#')
        fileTrainSale.write(tokens[0]+','+tokens[1]+','+tokens[2]+','+str(v)+'\n')
        conn.execute('insert into saleTrain values (?,?,?,?)', (tokens[0],tokens[1],tokens[2],str(v)))
    conn.commit()
    conn.close()
    print 'CustomSale created'
    
def appendMissingToCustomSale():
    conn = sqlite3.connect("data/asus.db")
    fileTrainSale = open("data/CustomTrainSale.csv", "a")
    with open('data/MissingSales.csv', 'rb') as csvfileS:
        sale = csv.reader(csvfileS, delimiter=',', quotechar=' ')
        for row in sale:
            fileTrainSale.write('M'+row[0]+','+'P'+row[1]+','+row[2]+'/'+row[3]+','+row[4]+'\n')
            conn.execute('insert into saleTrain values (?,?,?,?)', ('M'+row[0],'P'+row[1],row[2]+'/'+row[3],row[4]))
            #print 'M'+row[0]+','+'P'+row[1]+','+row[2]+'/'+row[3]+','+row[4]
        conn.commit()
        conn.close()
def createCustomRepair():
    conn = sqlite3.connect("data/asus.db")
    fileTrainRepair = open("data/CustomTrainRepair.csv", "wb")        
    data=[]
    with open('data/RepairTrain.csv', 'rb') as csvfileR:
        repair = csv.reader(csvfileR, delimiter=',', quotechar=' ')
        repair.next()
        for row in repair:
            data.append(row)
    print "Data loaded."    
    datasource = dict(enumerate(data))
        
    s = mincemeat.Server()
    s.datasource = datasource
    s.mapfn = mapfnRepair
    s.reducefn = reducefnRepair
    results = s.run_server(password="changeme")
    for k,v in results.items():
        tokens = k.split('#')
        fileTrainRepair.write(tokens[0]+','+tokens[1]+','+tokens[2]+','+tokens[3]+','+str(v)+'\n')
        conn.execute('insert into repairTrain values (?,?,?,?,?)', (tokens[0],tokens[1],tokens[2],tokens[3],str(v)))
    conn.commit()
    conn.close()
    print 'CustomRepair created'

# metodo alternativo a createTrain(), incluye los datos de sales que por fechas no cruzan con repair (left join)
# y como numero de ventas se incluye la media del componente y modulo
def createTrainDB():
    auxTrain = open("data/auxTrain.csv", "wb")
    conn = sqlite3.connect("data/asus.db")
    
    #crear dict con ventas medias por componente y modulo
    for row in conn.execute('select saleTrain.modulo, saleTrain.componente, avg(saleTrain.n_sale) from saleTrain group by saleTrain.modulo,saleTrain.componente'):
        #print row
        medias[str(row[0])+'#'+str(row[1])]=row[2]
    
    #fichero union de reparaciones y ventas (aun nulas) 
    for row in conn.execute('SELECT saleTrain.*, repairTrain.* FROM repairTrain LEFT OUTER JOIN saleTrain '+
                            'ON saleTrain.modulo = repairTrain.modulo AND '+
                            'saleTrain.componente = repairTrain.componente AND '+
                            'saleTrain.fec_sale = repairTrain.fec_sale '):
                            
        auxTrain.write(str(row[0])+','+str(row[1])+','+str(row[2])+','+str(row[3])+','+str(row[4])+','+str(row[5])+','+str(row[6])+','+str(row[7])+','+str(row[8])+'\n')
    auxTrain.close()
    
    #completar ventas nulas con la media de ventas del componente y modulo
    auxCustomTrain = open("data/CustomTrain.csv","wb")
    with open('data/auxTrain.csv', 'rb') as csvAux:
        aux = csv.reader(csvAux, delimiter=',', quotechar=' ')
        for row in aux:
            if row[3] != 'None':
                ventas = row[3]
            
            else:
                clave = row[4]+'#'+row[5]
                ventas = str(int(medias[clave]))
            
            if row[6].__len__() == 6:
                mes_venta = row[6][5:6]
            elif row[6].__len__() == 7:
                mes_venta = row[6][5:7]
            if row[7].__len__() == 6:
                mes_repar = row[7][5:6]
            elif row[7].__len__() == 7:
                mes_repar = row[7][5:7]    
            #n_reparac, tipo, modelo, anio_venta, mes_venta, anio_reparacion, mes_reparacion, n_ventas
            linea = row[8]+','+row[4][1:3]+','+row[5][1:3]+','+row[6][0:4]+','+mes_venta+','+ventas+','+row[7][0:4]+','+mes_repar+'\n' 
            auxCustomTrain.write(linea)
     
        '''
        'UNION ALL '+
        'SELECT saleTrain.*, repairTrain.* FROM repairTrain LEFT OUTER JOIN saleTrain '+
        'ON saleTrain.modulo = repairTrain.modulo AND '+
        'saleTrain.componente = repairTrain.componente AND '+
        'saleTrain.fec_sale = repairTrain.fec_sale'):
        '''
# metodo alternativo a createTrainDB(), solo incluye sales que cruzan con repair por fechas                      
def createTrain():           
    fileTrain = open("data/CustomTrain.csv", "wb")
    with open('data/CustomTrainSale.csv', 'rb') as csvfileS:
        sale = csv.reader(csvfileS, delimiter=',', quotechar=' ')
        for rowS in sale:
            with open('data/CustomTrainRepair.csv','rb') as csvfileR:
                repair = csv.reader(csvfileR, delimiter=',', quotechar=' ')
                for rowR in repair:
                    if rowR[0] == rowS[0] and rowR[1] == rowS[1] and rowR[2] == rowS[2]:
                        #n_reparac, tipo, modelo, anio_venta, mes_venta, anio_reparacion, mes_reparacion, n_ventas
                        if rowR[2].__len__() == 6:
                            mes_venta = rowR[2][5:6]
                        elif rowR[2].__len__() == 7:
                            mes_venta = rowR[2][5:7]
                        if rowR[3].__len__() == 6:
                            mes_repar = rowR[3][5:6]
                        elif rowR[3].__len__() == 7:
                            mes_repar = rowR[3][5:7]    
                        linea = rowR[4]+','+rowR[0][1:3]+','+rowR[1][1:3]+','+rowR[2][0:4]+','+mes_venta+','+str(abs(int(rowS[3])))+','+rowR[3][0:4]+','+mes_repar+'\n'
                        fileTrain.write(linea) 
              
        print ("Fichero CustomTrain creado.")

                        
def createTest():
    fileTest = open('data/CustomTest.csv','wb')
    with open('data/Output_TargetID_Mapping.csv') as csvfileO:
        output = csv.reader(csvfileO,delimiter=',', quotechar=' ')
        for rowO in output:
            with open('data/CustomTrainSale.csv') as csvfileS:
                sale = csv.reader(csvfileS, delimiter=',', quotechar=' ')
                for rowS in sale:
                    if rowS[0] == rowO[0] and rowS[1] == rowO[1]:
                        if rowS[2].__len__() == 6:
                            mes_venta = rowS[2][5:6]
                        elif rowS[2].__len__() == 7:
                            mes_venta = rowS[2][5:7]
                            
                        # tipo, modelo, anio_venta, mes_venta, n_ventas, anio_reparacion, mes_reparacion, 
                        linea = rowO[0][1:2]+','+rowO[1][1:3]+','+rowS[2][0:4]+','+mes_venta+','+rowS[3]+','+rowO[2]+',' +rowO[3]+'\n'
                        fileTest.write(linea)
        
        print ("Fichero CustomTest creado.")

def predict():
    Prediction.predict()
   
def mapResults(k,v):
    #tipo modelo anio_repair mes_repair, n_repair
    print v[1]+'#'+v[2]+'#'+v[6]+'#'+v[7]+'#'+v[0]
    yield v[1]+'#'+v[2]+'#'+v[6]+'#'+v[7],int(v[0])
    
def reduceResults(k,vs):
    result = sum(vs)
    return result

def selectResults():
    fileSelectResults = open('data/SelectedResults.csv','wb')
    data=[]
    with open('data/Results.csv', 'rb') as csvfileR:
        results = csv.reader(csvfileR, delimiter=',', quotechar=' ')
        for row in results:
            data.append(row)  
    datasource = dict(enumerate(data))
        
    s = mincemeat.Server()
    s.datasource = datasource
    s.mapfn = mapResults
    s.reducefn = reduceResults
    results = s.run_server(password="changeme")
    i=1
    for k,v in results.items():
        tokens = k.split('#')
        fileSelectResults.write(tokens[0]+','+tokens[1]+','+tokens[2]+','+tokens[3]+','+str(v)+'\n')
        i+=1

def createSubmission():
    fileSubmit = open('data/Submit.csv','wb')
    fileSubmit.write('id,target\n')
    cont=1
    with open('data/Output_TargetID_Mapping.csv','rb') as outputFile:
        outputFile.next()   #no procesar cabecera
        output = csv.reader(outputFile,delimiter=',', quotechar=' ')
        for rowO in output:
            with open('data/SelectedResults.csv','rb') as resultsFile:
                results = csv.reader(resultsFile,delimiter=',', quotechar=' ')
                for rowR in results:
                    compo = rowR[1]
                    if compo.__len__() < 2:
                        compo = '0'+compo
                    if rowO[0][1:2] == rowR[0] and rowO[1][1:3] == compo and rowO[2] == rowR[2] and rowO[3] == rowR[3]:
                        fileSubmit.write(str(cont)+','+rowR[4]+'\n')
                        cont+=1
        
def main():
      
    DB.main()               # execute DB.py to remove B.D.
    createCustomSale()      # Map/Reduce
    createCustomRepair()    # Map/Reduce
    createTrainDB()        # left join sales x repair: para incluir media de ventas de datos no facilitados
    createTest()
    predict()
    selectResults()         # Map/Reduce
    createSubmission()
'''
def main():
    DB.main()               # execute DB.py to remove B.D.
    createCustomSale()      # Map/Reduce
    MissingSales.main()     # execute MissingSales for appending Missing Data  
    appendMissingToCustomSale()
    createCustomRepair()    # Map/Reduce
    createTrain()
    createTest()
    predict()
    selectResults()         # Map/Reduce
    createSubmission()
'''    
if __name__=="__main__":
    main()
    