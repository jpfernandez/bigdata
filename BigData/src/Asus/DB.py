import sqlite3

def createDB():
    c = sqlite3.connect("data/asus.db")
    #c = sqlite3.connect(':memory:')
    # Create table
    
    c.execute('''DROP TABLE saleTrain ''')
    c.execute('''DROP TABLE repairTrain ''')
    
    c.execute('''CREATE TABLE saleTrain
             (modulo text, 
             componente text, 
             fec_sale text,
             n_sale integer,
             PRIMARY KEY ( modulo, componente, fec_sale))''')

    c.execute('''CREATE TABLE repairTrain
             (modulo text,
             componente text,
             fec_sale text,
             fec_repair text,
             n_repair integer,
             PRIMARY KEY ( modulo, componente, fec_sale, fec_repair))''')
             

    # Save (commit) the changes
    c.commit()
    c.close()

def main():
    createDB()

if __name__=="__main__":
    main()