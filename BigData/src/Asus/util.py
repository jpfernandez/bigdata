from pylab import plot,show
from numpy import arange,array,ones,linalg
from sklearn import datasets, linear_model
import LoadData

rdo = []
rdo = LoadData.loadData()
X = rdo.__getitem__(0)
    
reparaciones = X[ : ,0]
ventas = X[ : ,5]


xi = arange(0,ventas.__len__())
A = array([ xi, ventas])
# linearly generated sequence
#y = [19, 20, 20.5, 21.5, 22, 23, 23, 25.5, 24]
#A = ventas
y = reparaciones
w = linalg.lstsq(A.T,y)[0] # obtaining the parameters

# plotting the line
line = w[0]*xi+w[1] # regression line
plot(xi,line,'r-',xi,y,'o')
show()