import csv as csv
import numpy as np

def loadDataForMissingSales():
    csv_file_object = csv.reader(open('data/MissingTrainSalesTrain.csv', 'rb')) 
    #header = csv_file_object.next()  #The next() command just skips the first line which is a header
    data=[]                          #Create a variable called 'data'
    for row in csv_file_object:      #Run through each row in the csv file
        data.append(row)             #adding each row to the data variable
    train = np.array(data)              #Then convert from a list to an array be aware that each item is currently a string in this format    
    
    csv_file_object = csv.reader(open('data/MissingTestSalesTrain.csv', 'rb')) 
    #header = csv_file_object.next()  #The next() command just skips the first line which is a header   data=[]                          #Create a variable called 'data'
    data=[]
    for row in csv_file_object:      #Run through each row in the csv file
        data.append(row)             #adding each row to the data variable
    test = np.array(data)              #Then convert from a list to an array be aware that each item is currently a string in this format
    
    return [train,test]

def loadData():
    #csv_file_object = csv.reader(open('data/auxCustomTrain.csv', 'rb')) 
    csv_file_object = csv.reader(open('data/CustomTrain.csv', 'rb'))
    #header = csv_file_object.next()  #The next() command just skips the first line which is a header
    data=[]                          #Create a variable called 'data'
    for row in csv_file_object:      #Run through each row in the csv file
        data.append(row)             #adding each row to the data variable
    data = np.array(data)              #Then convert from a list to an array be aware that each item is currently a string in this format    
    #train = data.copy()
    train = np.memmap('data/memTrain', dtype='int32', mode='r+', shape=(data.shape[0],data.shape[1]))
    train[:] = data[:]

    csv_file_object = csv.reader(open('data/CustomTest.csv', 'rb')) 
    #header = csv_file_object.next()  #The next() command just skips the first line which is a header   data=[]                          #Create a variable called 'data'
    data=[]
    for row in csv_file_object:      #Run through each row in the csv file
        data.append(row)             #adding each row to the data variable
    data = np.array(data)              #Then convert from a list to an array be aware that each item is currently a string in this format
    test = np.memmap('data/memTest', dtype='int32', mode='r+', shape=(data.shape[0],data.shape[1]))
    test[:] = data[:]
    
    return [train,test]

