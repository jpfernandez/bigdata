'''
    Predecir la ventas para los datos de SaleTrain no facilitados.
'''
import Prediction
import csv as csv

def createTestForMissingTrainSales():
    fileTestMissing = open("data/MissingTestSalesTrain.csv", "wb")
    modulos = ['M0','M1','M2','M3','M4','M5','M6','M7','M8','M9',]
    componentes = ['P01','P02','P03','P04','P05','P06','P07','P08','P09','P10','P11','P12','P13','P14','P15','P16','P17','P18','P19','P20','P21',
                   'P22','P23','P24','P25','P26','P27','P28','P29','P30','P31']
    fechas = ['2008/3','2008/4','2008/5','2008/6','2008/7','2008/8','2008/9','2008/10','2008/11','2008/12',
              '2009/1','2009/2','2009/3','2009/4','2009/5','2009/6','2009/7','2009/8','2009/9','2009/10','2009/11','2009/12']
                
              #'2010/1','2010/2','2010/3','2010/4','2010/5','2010/6','2010/7','2010/8','2010/9','2010/10','2010/11','2010/12',
              #'2011/1','2011/2','2011/3','2011/4','2011/5','2011/6','2011/7']
              
    for m in modulos:
        for c in componentes:
            for f in fechas:
                if f.__len__() == 6:
                    mes = f[5:6]
                elif f.__len__() == 7:
                    mes = f[5:7]
                fileTestMissing.write(m[1:2]+','+c[1:3]+','+f[0:4]+','+mes+'\n')

def createTrainForMissingTrainSales():
    fileTrainMissing = open('data/MissingTrainSalesTrain.csv','wb')
    with open("data/CustomTrainSale.csv", "rb") as customTrainSale:
        sale = csv.reader(customTrainSale, delimiter=',', quotechar=' ')
        for row in sale:
            if row[2].__len__() == 6:
                mes = row[2][5:6]
            elif row[2].__len__() == 7:
                mes = row[2][5:7]
            fileTrainMissing.write(row[3]+','+row[0][1:2]+','+row[1][1:3]+','+row[2][0:4]+','+mes+'\n');
    
def createMissingSales(data):
    i=0
    file = open('data/MissingSales.csv','wb')
    with open("data/MissingTestSalesTrain.csv", "rb") as test:
        t = csv.reader(test, delimiter=',', quotechar=' ')
        for row in t:
            file.write(row[0]+','+row[1]+','+row[2]+','+row[3]+','+data[i]+'\n')
            i+=1
    
def main():
        
    createTestForMissingTrainSales()
    createTrainForMissingTrainSales()
    
    data = []
    data = Prediction.predictMissingSales()
    createMissingSales(data)

if __name__=="__main__":
    main()        
    