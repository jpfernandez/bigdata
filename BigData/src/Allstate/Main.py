import numpy as np
from numpy import genfromtxt, savetxt
import itertools
#from sklearn.preprocessing import Imputer


def main():
    # grupos de coberturas A..G con sus garantias
    coberturas = [[0,1,2],[0,1],[1,2,3,4],[1,2,3],[0,1],[0,1,2,3],[1,2,3,4]]
    dictCober = dict()
    cont = 0
    # producto cartesiano de coberturas y garantias en un dict k=secuenciasl,v=garantias
    for element in itertools.product(*coberturas):
        dictCober[cont] = ''.join(str(e) for e in element)
        cont+=1
    # codificacion de car_value
    car_values = {'a':0,'b':1,'c':2,'d':3,'e':4,'f':5,'g':6,'h':7,'i':8}
    dataset = genfromtxt(open('data/train.csv','r'), delimiter=',', dtype='int64')[1:10000]
    
    coberturas_codificadas = []
    train_data = dataset[ : ,0:17]
    # columna coste en la posicion 17
    train_data = np.insert(train_data,17,dataset[ : ,24],1)
        

    # identificar las garantias de cada fila(A..G) con el secuencial del dict
    for row in dataset[ : ,17:24]:
        a = ''.join(str(r) for r in row)
        key = dictCober.keys()[dictCober.values().index(a)]
        coberturas_codificadas.append(key)
    train_data = np.insert(train_data,18,values=coberturas_codificadas, axis=1)
    
    # estimar car_value vacios
    car_value_train = dataset[dataset[ : ,10]]
    car_value_test = dataset[not dataset[ : ,10]]
    
    print car_value_train
    print car_value_test
    
    # identificar el car_value con el valor del dict car_values

    '''
    array_car_values = []
    print dataset[ : ,10].shape
    for item in dataset[ : ,10]:
        if item:
            value = np.int(car_values[item])
        else:
            value = np.NaN
        array_car_values.append(value)
    
    print array_car_values.__len__()
    '''
    '''
    # imputar datos en blanco de array_car_values con mean
    imp = Imputer(strategy='most_frequent',missing_values=np.NaN,axis=0)
    a = imp.fit(dataset[ : ,9]).transform(array_car_values)
    print a.shape
    #print array_car_values
    train_data = np.delete(train_data, np.s_[10], 1)
    train_data = np.insert(train_data,10,values=array_car_values,axis=1)
    '''
    '''
    withValue = open('data/withValue.csv','wb')
    withoutValue = open('data/withoutValue.csv','wb')
    for row in train_data:
        if row[10] != '':
            withValue.write(str(row[0:18])+'\n')
        else:
            withoutValue.write(str(row[0])+'\n')
    #value = car_values[item]
    #array_car_values.append(value)
    
    
    '''
    '''
     # imputar datos en blanco con mean
    imp = Imputer(missing_values='NA', strategy='mean', axis=0)
    imp.fit(train_data[ : ,11])
    train_data = imp.transform(train_data[ : ,11].values)
    '''
    
  
    #savetxt('data/aux.csv', train_data[0:5], delimiter=',', fmt='%s')     
        
    
if __name__=="__main__":
    main()    