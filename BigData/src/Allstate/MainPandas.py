import numpy as np
import pandas as pd
from pandasql import sqldf
import itertools
from pandas.core.common import isnull
from sklearn.ensemble.forest import RandomForestClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.cross_validation import cross_val_score
import sklearn.feature_selection as fs
from sklearn import svm
import csv as csv
import numpy as np

# global variables
cols = ['customer_ID',# A unique identifier for the customer
        #'shopping_pt',# Unique identifier for the shopping point of a given customer
        #'record_type', # 0=shopping point, 1=purchase point
        'day', # Day of the week (0-6, 0=Monday)
        #'time', # Time of day (HH:MM)
        'state', # State where shopping point occurred
        'location', # Location ID where shopping point occurred
        'group_size', # How many people will be covered under the policy (1, 2, 3 or 4)
        'homeowner', # Whether the customer owns a home or not (0=no, 1=yes)
        'car_age', # Age of the customer's car
        'car_value', # How valuable was the customer's car when new
        'risk_factor', # An ordinal assessment of how risky the customer is (1, 2, 3, 4)
        'age_oldest', # Age of the oldest person in customer's group
        'age_youngest', # Age of the youngest person in customer's group
        'married_couple', # Does the customer group contain a married couple (0=no, 1=yes)
        'C_previous', # What the customer formerly had or currently has for product option C (0=nothing, 1, 2, 3,4)
        'duration_previous', #  how long (in years) the customer was covered by their previous issuer
        'A','B','C','D','E','F',
        'G', # the coverage options
        'cost']
# grupos de coberturas A..G con sus garantias
coberturas = [[0,1,2],[0,1],[1,2,3,4],[1,2,3],[0,1],[0,1,2,3],[1,2,3,4]]
dictCober = dict()
# codificacion de car_value
car_values = {'a':0,'b':1,'c':2,'d':3,'e':4,'f':5,'g':6,'h':7,'i':8}
# codificacion state
states = {'AL':0,'AR':1,'CO':2,'CT':3,'DC':4,'DE':5,'FL':6,'GA':7,'IA':8,'ID':9,'IN':10,'KS':11,'KY':12,'MD':13,
'ME':14,'MO':15,'MS':16,'MT':17,'ND':18,'NE':19,'NH':20,'NM':21,'NV':22,'NY':23,'OH':24,'OK':25,'OR':26,
'PA':27,'RI':28,'SD':29,'TN':30,'UT':31,'WA':32,'WI':33,'WV':34,'WY':35}

def fillValues(tipo):
    df = pd.read_csv('data/'+tipo+'.csv', sep=',', header=0)#[0:10000] #names=cols
    
    cober = np.array(df[['A','B','C','D','E','F','G']][1:])
    coberturas_codificadas = []
    for row in cober:
        a = ''.join(str(r) for r in row)
        key = dictCober.keys()[dictCober.values().index(a)]
        coberturas_codificadas.append(key)
        
    #df['coberturas'] = pd.Series(coberturas_codificadas)
    
    # valores NaN de C_previous y duration_revious con mean()
    #df = df.fillna(df.mean()['C_previous':'duration_previous'])
    df['C_previous'] = df['C_previous'].fillna(0)
    df['duration_previous'] = df['duration_previous'].fillna(0)
    df['location'] = df['location'].fillna(10000)
    
    # estimar valores de car_value vacios 
    #df['car_value'] = df['car_value'].apply(lambda x: np.int(car_values[x]) if not pd.isnull(x) else np.NaN)
    testCarValue = df.ix[df['car_value'].map(lambda x: pd.isnull(x))]
    trainCarValue = df.ix[df['car_value'].map(lambda x: not pd.isnull(x))]
    
    columns = ["customer_ID", "record_type","day","group_size","homeowner","car_age","age_oldest","age_youngest",
               "married_couple","C_previous","duration_previous"]
 
    labels = trainCarValue["car_value"].values      # y
    features = trainCarValue[list(columns)].values  # X
    clf = RandomForestClassifier(n_estimators=100,random_state=1512)
    #print cross_val_score(clf, features, labels, n_jobs=-1).mean()
    
    clf.fit(features,labels)
    out = clf.predict(testCarValue[list(columns)].values)
    #testCarValue = testCarValue.drop(['car_value'],1)
    out1 = []
    for item in out:
        out1.append(item)
    testCarValue['car_value'] = np.array(out1)
    aux = trainCarValue.append(testCarValue)
    aux.to_csv('data/'+tipo+'WithCarValues.csv', index=False)
    df = aux
    
    #estimar valores de risk_factor = NA
    testRiskFactor = df.ix[df['risk_factor'].map(lambda x: pd.isnull(x))]
    trainRiskFactor = df.ix[df['risk_factor'].map(lambda x: not pd.isnull(x))]
    columns = ["customer_ID", "record_type","day","group_size","homeowner","car_age","age_oldest","age_youngest",
               "married_couple"]
 
    labels = trainRiskFactor["risk_factor"].values      # y
    features = trainRiskFactor[list(columns)].values  # X
    print cross_val_score(clf, features, labels).mean()
    clf.fit(features,labels)
    out = clf.predict(testRiskFactor[list(columns)].values)
    out2 = []
    for item in out:
        out2.append(item)
    testRiskFactor['risk_factor'] = np.array(out2)
    aux = trainRiskFactor.append(testRiskFactor)
    aux.to_csv('data/'+tipo+'WithCarValuesRiskFactor.csv', index=False,sep=',')
    clf = None

def predict():
    train = pd.read_csv('data/trainWithCarValuesRiskFactor.csv', sep=',', header=0)[0:10000] 
    test = pd.read_csv('data/test_v2WithCarValuesRiskFactor.csv', sep=',', header=0)[0:10000]
    # quitar columnas innecesarias
    train = train.drop(['time'],1)
    test = test.drop(['time'],1)
    #train = train.drop(['shopping_pt'],1)
    #test = test.drop(['shopping_pt'],1)
    
    # codificar valores tipo string
    train['car_value'] = train['car_value'].map(lambda x: car_values[x])
    test['car_value'] = test['car_value'].map(lambda x: car_values[x])
    train['state'] = train['state'].map(lambda x: states[x])
    test['state'] = test['state'].map(lambda x: states[x])
    # predict
    labels = train['record_type'].values    # y
    features = train[list(cols)].values     # X
    
    clf = GaussianNB()
    #clf = svm.SVC()
    #clf = RandomForestClassifier(n_estimators=100,random_state=1512) #0.85
    
    # feature selection
    b = fs.SelectKBest(fs.chi2, k=23) #k is number of features.
    b.fit(features, labels)
    
    print b._pvalues
    print b._scores
    
    print cross_val_score(clf, features, labels, n_jobs=-1).mean()
    
    clf.fit(features,labels)
    
    out = clf.predict(test[list(cols)].values)
    probs = clf.predict_proba(test[list(cols)].values)
    test['prediction'] = np.array(out)
    test['prob_pos'] = np.array(probs[ :,0])
    test['prob_neg'] = np.array(probs[ :,1])
        
    
    # seleccionar el mas actual
    #recent = '''
    #select * from test t where shopping_pt = (select max(shopping_pt) from test t2 where
    #                                            t.customer_ID = t2.customer_ID)
    #'''
    
    
    #currentVSlast = 
    '''
    select * from test t where t.prediction = 1
    union
    select * from test t where t.prediction = 0 and 
        t.shopping_pt = (select max(t2.shopping_pt) from test t2 where
        t.customer_ID = t2.customer_ID) and 
        t.customer_ID not in (select customer_ID from test where prediction = 1);
    '''
    currentVSlast = '''
    select * from test t where t.prediction = 1
    union
    select * from test t where t.prediction = 0 and 
        t.cost = (select min(cost) from test group by customer_ID)  and 
        t.customer_ID not in (select customer_ID from test where prediction = 1);
    '''
    
    # si prediction = 0 => seleccinar el de menor prob_neg; 
    # si prediction = 1 => seleccionar el de mayor prob_pos
    probPosVSprobNeg = '''
    select *
    from test t
    where prediction = 1 and
    prob_pos = (select max(prob_pos) from test t2 where t.customer_ID = t2.customer_ID)
    group by customer_ID
    union
    select *
    from test t
    where prediction = 0 and
    prob_neg = (select min(prob_neg) from test t2 where t.customer_ID = t2.customer_ID)
    group by customer_ID   
    '''
    
    PosVSNeg = sqldf(probPosVSprobNeg, locals())
    
    # si hay repetidos para el mismo customer_ID => seleccinar el de menor cost
    mincost = '''
    select customer_ID,A,B,C,D,E,F,G from PosVSNeg u where u.cost = (select min(t.cost) from PosVSNeg t where u.customer_ID = t.customer_ID); 
    '''
    result = sqldf(mincost,locals())
      
    #result = pd.merge(test,negs, left_on=['customer_ID', 'prob_neg'],right_on=['customer_ID', 'prob_neg'],how='inner')
    result = result.drop_duplicates('customer_ID')
    #convertir a string las columnas
    result['customer_ID'] = result['customer_ID'].apply(lambda x: str(int(x)))
    
    for col in ['A','B','C','D','E','F','G']:
        result[col] = result[col].apply(lambda x: str(int(x)))
    
    result['plan'] = result['A']+result['B']+result['C']+result['D']+result['E']+result['F']+result['G']
    
    for col in ['A','B','C','D','E','F','G']:
        result = result.drop([col],1)
    
    #result.to_csv('data/result.csv', index=False,sep=',',header=True)
    
    fileSubmit = open('data/submit.csv','wb')
    fileSubmit.write('customer_ID,plan\n')
    for index,row in result.iterrows():
        fileSubmit.write(str(row['customer_ID'])+','+str(row['plan'])+'\n')
        

def main():
    # producto cartesiano de coberturas y garantias en un dict k=secuenciasl,v=garantias
    sec = 0
    for element in itertools.product(*coberturas):
        dictCober[sec] = ''.join(str(e) for e in element)
        sec+=1
    
    #fillValues('train')
    #fillValues('test_v2')
    predict()

if __name__=="__main__":
    main() 