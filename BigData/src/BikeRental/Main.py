'''
Multioutput-multiclass classification and multi-task classification 

count = casual + registered

datetime - hourly date + timestamp  
season -  1 = spring, 2 = summer, 3 = fall, 4 = winter 
holiday - whether the day is considered a holiday
workingday - whether the day is neither a weekend nor holiday
weather - 1: Clear, Few clouds, Partly cloudy, Partly cloudy
2: Mist + Cloudy, Mist + Broken clouds, Mist + Few clouds, Mist
3: Light Snow, Light Rain + Thunderstorm + Scattered clouds, Light Rain + Scattered clouds
4: Heavy Rain + Ice Pallets + Thunderstorm + Mist, Snow + Fog 
temp - temperature in Celsius
atemp - "feels like" temperature in Celsius
humidity - relative humidity
windspeed - wind speed
casual - number of non-registered user rentals initiated
registered - number of registered user rentals initiated
count - number of total rentals
'''

import decimal
import time
from sklearn.cross_validation import cross_val_score
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import ExtraTreesClassifier
import numpy as np
import pandas as pd
import Helper as h
from sklearn.neighbors.classification import KNeighborsClassifier
from sklearn.naive_bayes import BernoulliNB


'''
    Mejorar esta funcion
'''    
def mesWeather(x,y):
    z = [0]*len(x)
    for i in range(0,len(x)):
        if x[i] in [9,10,11,12,1,2] and y[i] in [3,4]:   # malo/invierno
            z[i] = np.int(0)
        elif x[i] in [3,4,5,6,7,8] and y[i] in [1,2]: # bueno/estival
            z[i] = np.int(1)
        elif x[i] in [3,4,5,6,7,8] and y[i] in [3,4]: # malo/estival
            z[i] = np.int(2)
        elif x[i] in [9,10,11,12,1,2] and y[i] in [1,2]: # bueno/inviero
            z[i] = np.int(3)
        else:
            z[i] = np.int(4)
    #z = np.array(z,dtype=np.int)
    
    return pd.Series(z)

def partDay(x):
    z = [0]*len(x)
    for i in range(0,len(x)):
        if x[i] in [7,8,9,10]:
            z[i] = np.int(0)
        elif x[i] in [11,12,13,14,15]:
            z[i] = np.int(1)    
        elif x[i] in [16,17,18,19,20]:
            z[i] = np.int(2)
        else:
            z[i] = np.int(3)
    return pd.Series(z)

def transform(df):
    date_format = "%Y-%m-%d %H:%M:%S"
    df['anio'] = df['datetime'].apply(lambda x: time.strptime(x,date_format).tm_year)
    df['mes'] = df['datetime'].apply(lambda x: time.strptime(x,date_format).tm_mon)
    df['dia'] = df['datetime'].apply(lambda x: time.strptime(x,date_format).tm_mday)
    df['hora'] = df['datetime'].apply(lambda x: time.strptime(x,date_format).tm_hour)
    df['temp'] = df['temp'].apply(lambda x : round(decimal.Decimal(x),2))
    df['atemp'] = df['atemp'].apply(lambda x : round(decimal.Decimal(x),2))
    #df['mes-weather'] = mesWeather(df["mes"],df["weather"])
    df['inc'] = df['atemp'] - df['temp']
    df['part_of_day'] = partDay(df['hora'])
    df['weekend'] = df["holiday"] + df["workingday"]
    df['season-weather'] = df['season']+df['weather']
    
    
    #dfTest['temp'] = dfTest['temp'].apply(lambda x : x.replace('.',''))
    #dfTest['atemp'] = dfTest['atemp'].apply(lambda x : x.replace('.',''))    
    
    return df

def classify(dfTrain,dfTest,feature):
    
    columns = ["season-weather","weekend","part_of_day","inc","mes","dia","hora","season","weather","temp","atemp","humidity","windspeed"]
    
    #labels = dfTrain[["casual","count"]].values      # y
    labels = feature.values      # y
    features = dfTrain[list(columns)].values  # X
    to_predict = dfTest[list(columns)] # Z
    # classifiers
    clf = RandomForestClassifier(n_estimators=10,max_depth=None,min_samples_split=1,random_state=0)
    #clf = ExtraTreesClassifier(n_estimators=20,random_state=0)
        
    print("%s %s" % (type(clf).__name__ ,cross_val_score(clf, features, labels, n_jobs=-1).mean()))

    #Print the feature ranking
    clf.fit(features, labels)
    rdo = clf.predict(to_predict);
    
    importances = clf.feature_importances_
    print importances
        #std = np.std([tree.feature_importances_ for tree in et.estimators_],axis=0)
    indices = np.argsort(importances)[::-1]
        #print("Feature ranking classifier: %s" % type(clf).__na5e__)
    
    for f in range(columns.__len__()):
        print("%d. %s (%f)" % (f + 1, columns[indices[f]], importances[indices[f]]))
    
    return rdo    

'''
    Train dataset tiene datos del dia 1 al 19 de cada mes y Test dataset tiene datos del dia 20 al 31 de cada mes:
    completar Train con una estimacion de datos del 20 al 31 teniendo en cuenta la media de los datos respecto al mes y hora
    OJO: no hay datos de Diciembre de 2012
'''        
def completeTrain(df):
    date_format = "%Y-%m-%d %H:%M:%S"
    aux = pd.DataFrame()
    dtime = []
    seasons = []
    holidays = []
    workingdays = []
    weather = []
    temp = []
    atemp = []
    humidity = []
    windspeed = []
    casual = []
    registered = []
    count = []
    for year in range(2011,2013):
        for month in range(1,13):
            for day in range(20,32):
                for hour in range(1,24):
                                                           
                    t = h.createDateTime(year, month, day, hour, 0, 0)
                    #dtime.append(str(year)+'-'+h.putZero(month)+'-'+h.putZero(day)+' '+h.putZero(hour)+':00:00')
                    dtime.append(t.strftime(date_format))
                    seasons.append(h.whichSeason(month))
                    holidays.append(h.isHoliday(t))
                    workingdays.append(h.isWorkingDay(t))
                    
                    r = df[(df.mes==month) & (df.hora==hour)] # datos agrupados por mes y hora
                    
                    weather.append(round(r['weather'].mean()))
                    temp.append(round(r['temp'].mean(),2))
                    atemp.append(round(r['atemp'].mean(),2))
                    humidity.append(round(r['humidity'].mean(),2))
                    windspeed.append(round(r['windspeed'].mean(),2))
                    casual.append(round(r['casual'].mean()))
                    registered.append(round(r['registered'].mean()))
                    count.append(round(r['casual'].mean()) + round(r['registered'].mean()))
                    
                    #print ("%s %s " % (day, round(r['atemp'].mean())))
                    
    aux['datetime'] = pd.Series(dtime)
    aux['season'] = pd.Series(seasons)
    aux['holiday'] = pd.Series(holidays)
    aux['workingday'] = pd.Series(workingdays)
    aux['weather'] = pd.Series(weather)
    aux['temp'] = pd.Series(temp)
    aux['atemp'] = pd.Series(atemp)
    aux['humidity'] = pd.Series(humidity)
    aux['windspeed'] = pd.Series(windspeed)
    aux['casual'] = pd.Series(casual)
    aux['registered'] = pd.Series(registered)
    aux['count']=pd.Series(count)
     
    aux = transform(aux)
    return df.append(aux)
    #return aux   


def main():
    dfTrain = pd.read_csv('data/train.csv', sep=',', header=0)
    dfTest = pd.read_csv('data/test.csv', sep=',', header=0)
    
    dfTrain = transform(dfTrain) 
    dfTrain = completeTrain(dfTrain) 
        
    dfTest = transform(dfTest)
    
    #h.analysis(dfTrain)
    
    registered = classify(dfTrain, dfTest,dfTrain["registered"])
    casual = classify(dfTrain, dfTest,dfTrain["casual"])
    
    dfTest["registered"] = pd.Series(registered,index=dfTest.index)
    dfTest["casual"] = pd.Series(casual,index=dfTest.index)
    dfTest["count"] = dfTest["registered"]+dfTest["casual"]
    dfTest["count"] = dfTest["count"].apply(lambda x: int(x))
    
    aux = dfTest[["datetime","count"]]
    aux.to_csv('data/submit.csv', index=False)
    
if __name__=="__main__":
    main()   
    
    