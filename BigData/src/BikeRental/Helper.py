import datetime
import matplotlib.pyplot as plt

def analysis(df):
    
    print sum(df['workingday'].values)
    print sum(df['holiday'].values)
    
    print df.head(5)
    print df['season-weather'].describe()
    print df.temp.unique()
    #print df.groupby('temp').mean().plot()
    #plt.hist(df.temp)
    
    fig = plt.figure()
    plt.scatter(df.registered, df.atemp, alpha=.3)
    axis = fig.gca()
    axis.set_title('Prueba')
    axis.set_xlabel('registered')
    axis.set_ylabel('atemp')
    #print correlation between temp and humidity
    plt.show()

def putZero(s):
    s = str(s)
    if len(s) == 1:
        s = '0'+s
    return s

def whichSeason(m):
    if m in [12,1,2]:
        return 4
    elif m in [3,4,5]:
        return 1
    elif m in [6,7,8]:
        return 2
    elif m in [9,10,11]:
        return 3
    
def isHoliday(t):
    if t.weekday() in [6,7]:
        return 1
    else:
        return 0

def isWorkingDay(t):
    if t.weekday() in [6,7]:
        return 0
    else:
        return 1

def createDateTime(y,m,d,h,mm,s):
    try:
        t = datetime.datetime(y,m,d,h,mm,s)
    except ValueError:
        try:
            t = datetime.datetime(y, m, d-1,h,mm,s)
        except ValueError:
            try:
                t = datetime.datetime(y, m, d-2,h,mm,s)
            except ValueError:
                t = datetime.datetime(y, m, d-3,h,mm,s)
    return t

