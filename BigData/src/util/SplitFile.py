'''
Created on 23/01/2014

@author: adm
'''

def main(path,zile,numSplits):
    f = open(path+zile)
    cont = 0
    while f.readline():
        cont +=1
    lines = cont / numSplits
    mod = cont % numSplits
    print 'total lines ' + chr(cont)
    print 'lines in file ' + chr(lines)
    print 'extra lines in last file ' + chr(mod)
    j=0
    f.seek(0)
    i=0
    while j < numSplits:
        fw = open(path+str(j)+'.csv','w')
        while i < lines:
            fw.write(f.readline())
            i+=1
        j+=1
        lines+=lines
        if j == numSplits:
            j = 0
            while j < mod:
                fw.write(f.readline())
        fw.close()        
    f.close()
    print 'Done!'
    
if __name__ == '__main__':
    main('../CIFAR/Data/','train.csv',20)