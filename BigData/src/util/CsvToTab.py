'''
Created on 05/09/2013

@author: indjfg
'''

def main():
    ifile  = open('test.csv', "rb")
    ofile  = open('ttest.tab', "wb")
    
    file_string = ifile.readlines()
    
    for row in file_string:
        line = row.replace("\t\t","\t?\t")
        ofile.write(line)    
    
    ifile.close()
    ofile.close()    
       
    print "finished!"

if __name__ == '__main__':
    main()