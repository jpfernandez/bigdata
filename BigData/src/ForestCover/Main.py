import numpy as np
import pandas as pd
import math
import matplotlib.pyplot as plt
from sklearn.ensemble.forest import RandomForestClassifier
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.naive_bayes import BernoulliNB
from sklearn import neighbors
from sklearn.cross_validation import cross_val_score
import sklearn.feature_selection as fs

def transform_data(df):
    def hipo(x):
        return math.sqrt(x[0]**2 + x[1]**2)
    
    # feature engineering
    # apply: Iter Rate each columns
    # aply axis = 1: Iter Rate each row
    # applymap: Iter Rate each item
    # map: Iter rate the items in this series

    df['Elevation_Diff'] = df['Elevation'].map(lambda x: 3849-x)
    df['Hydro_Road'] = abs(df['Horizontal_Distance_To_Hydrology'] +  df['Horizontal_Distance_To_Roadways'])
    df['Fire_Road'] = abs(df['Horizontal_Distance_To_Fire_Points'] + df['Horizontal_Distance_To_Roadways'])
    df['Fire_Hydro'] = abs(df['Horizontal_Distance_To_Fire_Points'] + df['Horizontal_Distance_To_Hydrology'])
    df['Fire_Hydro_Road'] = df['Horizontal_Distance_To_Fire_Points'] + df['Horizontal_Distance_To_Hydrology'] + df['Horizontal_Distance_To_Roadways']
    
    df['Elevation_Road'] = abs(df["Elevation"] + df["Horizontal_Distance_To_Roadways"])
    #df['Elevation_Road'] = df[["Elevation","Horizontal_Distance_To_Roadways"]].apply(hipo,axis=1)
    df['Elevation_Hydro'] = abs(df["Elevation"] + df["Horizontal_Distance_To_Hydrology"])
    #df['Elevation_Hydro'] = df[["Elevation","Horizontal_Distance_To_Hydrology"]].apply(hipo,axis=1)
    df['Elevation_Fire'] = abs(df["Elevation"] + df["Horizontal_Distance_To_Fire_Points"])
    #df['Elevation_Fire'] = df[["Elevation","Horizontal_Distance_To_Fire_Points"]].apply(hipo,axis=1)
    
    #df['shade_9am'] = df["Hillshade_9am"] / df["Slope"].apply(lambda x : math.tan(x) if math.tan(x)> 0 else 1)
    #df['shade_Noon'] = df["Hillshade_Noon"] / df["Slope"].apply(lambda x : math.tan(x) if math.tan(x)> 0 else 1)
    #df['shade_3pm'] = df["Hillshade_3pm"] / df["Slope"].apply(lambda x : math.tan(x) if math.tan(x)> 0 else 1)
    #df['sun'] = df["Aspect"].map(lambda x : 1 if x > 90 and x < 180 else 0)
    
    #df['Elevation_Slope'] = (df['Elevation'] + df['Slope']).map(lambda x: x/2)
    df['Elevation_Slope'] = df['Elevation'].map(lambda x: math.sqrt(x**2+30**2))
    #df['Elevation_Slope'] = df['Elevation'] / df["Slope"].map(lambda x : math.tan(x) if math.tan(x)> 0 else 1)
    
    #df['Vertical_Distance_To_Hydrology'] = df['Vertical_Distance_To_Hydrology'].map(lambda x: x + 146)
    #df['angles'] = (df['Aspect']+df['Slope']+df['Hillshade_9am']+df['Hillshade_Noon']+df['Hillshade_3pm']).map(lambda x: x /5)
    df['risk'] = abs(df['Elevation_Diff'] - df['Horizontal_Distance_To_Roadways'])
    return df
        
def main():
    # data loading
    dfTrain = pd.read_csv('data/train.csv', sep=',', header=0)#[0:10000]
    dfTest = pd.read_csv('data/test.csv', sep=',', header=0)#[0:10000]
    
    dfTrain = transform_data(dfTrain)
    dfTest = transform_data(dfTest)
    
    # classifier
    columns = ["Elevation", "Aspect","Slope","Horizontal_Distance_To_Hydrology","Vertical_Distance_To_Hydrology",
               "Horizontal_Distance_To_Roadways",
               #"Hillshade_9am","Hillshade_Noon","Hillshade_3pm",
               "Horizontal_Distance_To_Fire_Points",
               "Wilderness_Area1","Wilderness_Area2","Wilderness_Area3","Wilderness_Area4",
               "Soil_Type1","Soil_Type2","Soil_Type3","Soil_Type4","Soil_Type5","Soil_Type6",
               #"Soil_Type7",
               #"Soil_Type8",
               "Soil_Type9","Soil_Type10","Soil_Type11","Soil_Type12","Soil_Type13","Soil_Type14",
               "Soil_Type15",
               "Soil_Type16","Soil_Type17","Soil_Type18","Soil_Type19","Soil_Type20","Soil_Type21",
               "Soil_Type22","Soil_Type23","Soil_Type24","Soil_Type25","Soil_Type26","Soil_Type27","Soil_Type28","Soil_Type29","Soil_Type30",
               "Soil_Type31","Soil_Type32","Soil_Type33","Soil_Type34","Soil_Type35","Soil_Type36","Soil_Type37","Soil_Type38","Soil_Type39",
               "Soil_Type40",
               "Fire_Hydro_Road","Hydro_Road","Fire_Road","Fire_Hydro",
               "Elevation_Road","Elevation_Hydro","Elevation_Fire","Elevation_Slope",
               "risk"
               ]
    
    labels = dfTrain["Cover_Type"].values      # y
    features = dfTrain[list(columns)].values  # X
    
    et = ExtraTreesClassifier(n_estimators=200,random_state=0)

    '''
    rf = RandomForestClassifier(n_estimators=100,random_state=1512)
    nv = BernoulliNB()
    knn = neighbors.KNeighborsClassifier(15, weights="distance") #uniform
    
    # feature selection
    
    b = fs.SelectKBest(fs.chi2, k=columns.__len__()) #k is the number of features.
    features = b.fit_transform(features, labels)
    featuresY = b.transform(dfTest[list(columns)].values)
    print b.pvalues_
    print b.scores_
    '''
    
    for clf in [et]:
        print cross_val_score(clf, features, labels, n_jobs=-1).mean()
    
    et.fit(features, labels)
    
    #Print the feature ranking
    
    importances = et.feature_importances_
    std = np.std([tree.feature_importances_ for tree in et.estimators_],axis=0)
    indices = np.argsort(importances)[::-1]
    print("Feature ranking:")

    for f in range(columns.__len__()):
        print("%d. %s (%f)" % (f + 1, columns[indices[f]], importances[indices[f]]))

    # Prediction
    
    out = et.predict(dfTest[list(columns)].values)
    dfTest["Cover_Type"] = pd.Series(out,index=dfTest.index)
    aux = dfTest[["Id","Cover_Type"]]
    aux.to_csv('data/submit.csv', index=False)
    

if __name__=="__main__":
    main()     